from array import array
import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from scipy.optimize import curve_fit
import sys
import os
from colorama import init
from termcolor import colored
init()
from brokenaxes import brokenaxes

import copy


#All values are given in µm
xActive = 94053
yActive = 102570
cutDistance = 65
xMid = xActive/2
yMid = yActive/2
class Sensor():
#active sensor surface (aluminium mask) identical for __all__ Sensors
#Dicing angles vary
#
#            N2                                         N1
#   W1      _____________________________________________
#           | p4                North              p1 ^ |   E2
#           |                   94118                 | |
#           |                              :            |
#           |                              :            |
#           |                              :            |
#           |West                (0|0)     :        East|   102700
#           |                              :            |
#           |                              :            |
#           |                              :            |
#           |                              :            |
#           |                                           |
#   W2      |_p3_______________South_________________p2_|   E1
#
#          S1                                          S2

    def __init__( self, pId, pDicing, pOrientation, pRotation, pOffset, pColor, pBax ):
        print("New sensor, ID: " + pId)
        self.id         = pId
        self.orientation= pOrientation
        self.rotation   = pRotation * 10**-6
        self.offset     = pOffset
        self.color      = pColor
        self.bax        = pBax
        self.layer      = pOrientation in ["Standard","Rotated"]  #Layer = True -> Top sensor, False -> Bottom Sensor

        self.sensorSize = { "North":    {"nominal": xActive + 2 * cutDistance},\
                            "East":     {"nominal": yActive + 2 * cutDistance}, \
                            "South":    {"nominal": xActive + 2 * cutDistance}, \
                            "West":     {"nominal": yActive + 2 * cutDistance}}
        #Calculate all points necessary for plotting
        self.p = {}

        #Origin
        self.p["0"]  = [0,0]
        #Corner of active sensos surface
        self.p["p1"] = [ xMid, yMid ]
        self.p["p2"] = [ xMid,-yMid]
        self.p["p3"] = [-xMid,-yMid]
        self.p["p4"] = [-xMid,yMid]

        #Start and end point of arrow
        self.p["a1"] = [xMid-8,yMid-50]
        self.p["a2"] = [xMid-8,yMid-10]

        #Start and end points of visalisation strips
        self.p["i1"] = [ -xMid +  50, -yMid + 30 ]
        self.p["i2"] = [ -xMid +  50, +yMid - 30 ]
        self.p["i3"] = [ -xMid + 140, -yMid + 30 ]
        self.p["i4"] = [ -xMid + 140, +yMid - 30 ]
        self.p["i5"] = [ +xMid - 140, -yMid + 30 ]
        self.p["i6"] = [ +xMid - 140, +yMid - 30 ]
        self.p["i7"] = [ +xMid -  50, -yMid + 30 ]
        self.p["i8"] = [ +xMid -  50, +yMid - 30 ]


        #Calculate the sensor dicing edges with respect to the aluminium mask
        #Sign of slopes must be changed. A positive slope in the mask alignment
        #measurement means distnces that get wider. Here this means the edge
        #must move further away along the edge and thus into negative y direction
        #afterwards the side is rotated accordingly to be on the right position
        #-----------------------------------------------------------------------
        #North
        if "North" in pDicing.keys():
            m = pDicing["North"]["m"]
            b = pDicing["North"]["b"]
        else:
            m = 0
            b = 0
        y1 = -10**-6 * m * xMid    - (yMid + b)
        y2 = -10**-6 * m * (-xMid) - (yMid + b)

        #Nominal linear function result in these positions "in front of lASER unit"
        #self.n1 = [-xMid, y2]
        #self.n2 = [xMid, y1]
        #Rotation matrix for CCW 180°:
        #   | -1  0  |
        #   | 0  -1  |
        #yields this vectors
        self.p["n1"] = [xMid, -y2]
        self.p["n2"] = [-xMid, -y1]

        #-----------------------------------------------------------------------
        #East
        if "East" in pDicing.keys():
            m = pDicing["East"]["m"]
            b = pDicing["East"]["b"]
        else:
            m = 0
            b = 0

        y1 = -10**-6 * m * yMid    - (xMid + b)
        y2 = -10**-6 * m * (-yMid) - (xMid + b)

        #Nominal linear function result in these positions "in front of lASER unit"
        #self.e1 = [-yMid, y2]
        #self.e2 = [yMid, y1]
        #Rotation matrix for CCW 90°:
        #   | 0  -1  |
        #   | 1   0  |
        #yields this vectors
        self.p["e1"] = [-y2,-yMid]
        self.p["e2"] = [-y1,yMid]


        #-----------------------------------------------------------------------
        #South
        if "South" in pDicing.keys():
            m = pDicing["South"]["m"]
            b = pDicing["South"]["b"]
        else:
            m = 0
            b = 0

        y1 = -10**-6 * m * xMid    - (yMid + b)
        y2 = -10**-6 * m * (-xMid) - (yMid + b)
        self.p["s1"] = [-xMid, y2]
        self.p["s2"] = [xMid, y1]
        #-----------------------------------------------------------------------
        #West
        if "West" in pDicing.keys():
            m = pDicing["West"]["m"]
            b = pDicing["West"]["b"]
        else:
            m = 0
            b = 0

        y1 = -10**-6 * m * yMid    - (xMid + b)
        y2 = -10**-6 * m * (-yMid) - (xMid + b)

        #Nominal linear function result in these positions "in front of lASER unit"
        #self.w1 = [-yMid, y2]
        #self.w2 = [yMid, y1]
        #Rotation matrix for CCW 270°:
        #   |  0  1  |
        #   | -1  0  |
        #yields this vectors
        self.p["w1"] = [y2,yMid]
        self.p["w2"] = [y1,-yMid]


        self.sensorSize["North"]["measured"] = self.p["e2"][0] - self.p["w1"][0]
        self.sensorSize["East"]["measured"]  = self.p["n1"][1] - self.p["s2"][1]
        self.sensorSize["South"]["measured"] = self.p["e1"][0] - self.p["w2"][0]
        self.sensorSize["West"]["measured"]  = self.p["n2"][1] - self.p["s1"][1]


        print("\nSizes\tNominal\tMeasured\tDifference")
        print("________________________________________________")
        for cardinal in self.sensorSize.keys():
            self.sensorSize[cardinal]["difference"] = self.sensorSize[cardinal]["nominal"] - self.sensorSize[cardinal]["measured"]
            print(cardinal + "\t" + str(self.sensorSize[cardinal]["nominal"]) + "\t" + str(self.sensorSize[cardinal]["measured"]) + "\t" + str(self.sensorSize[cardinal]["difference"]))
        print("\n")
    def drawSensor( self, pActiveArea = True, pStrips = True, pDicingEdges = True, pDrawLabels = True ):
        p = self.createProjection(self.p)

        if self.layer:
            self.bax.text(xMid+50, yMid+50, self.id, color = self.color)
        else:
            self.bax.text(xMid+50, yMid+20, self.id,color = self.color)

        if pActiveArea:
            self.bax.plot([p["p1"][0],p["p2"][0]],[p["p1"][1],p["p2"][1]],  linestyle='-.', color = self.color)
            self.bax.plot([p["p2"][0],p["p3"][0]],[p["p2"][1],p["p3"][1]],  linestyle='-.', color = self.color)
            self.bax.plot([p["p3"][0],p["p4"][0]],[p["p3"][1],p["p4"][1]],  linestyle='-.', color = self.color)
            self.bax.plot([p["p4"][0],p["p1"][0]],[p["p4"][1],p["p1"][1]],  linestyle='-.', color = self.color)

        #draw the arrow north east
        self.bax.arrow(p["a1"][0], p["a1"][1], p["a2"][0]-p["a1"][0],p["a2"][1]-p["a1"][1], head_width= 5,color = self.color  )

        #draw four readout strips, to get a feeling on the actula missalignment
        if pStrips:
            self.bax.plot([p["i1"][0],p["i2"][0]], [p["i1"][1],p["i2"][1]],  linestyle=':', color = self.color)
            self.bax.plot([p["i3"][0],p["i4"][0]], [p["i3"][1],p["i4"][1]],  linestyle=':', color = self.color)
            self.bax.plot([p["i5"][0],p["i6"][0]], [p["i5"][1],p["i6"][1]],  linestyle=':', color = self.color)
            self.bax.plot([p["i7"][0],p["i8"][0]], [p["i7"][1],p["i8"][1]],  linestyle=':', color = self.color)

        #dicing edges
        if pDicingEdges and False:
            self.bax.plot([p["n1"][0], p["n2"][0]],[p["n1"][1], p["n2"][1]],  linestyle=':', color = self.color)
            self.bax.plot([p["e1"][0], p["e2"][0]],[p["e1"][1], p["e2"][1]],  linestyle=':', color = self.color)
            self.bax.plot([p["s1"][0], p["s2"][0]],[p["s1"][1], p["s2"][1]],  linestyle=':', color = self.color)
            self.bax.plot([p["w1"][0], p["w2"][0]],[p["w1"][1], p["w2"][1]],  linestyle=':', color = self.color)

        #Extrpolated lines
        if pDicingEdges:
            self.bax.plot([p["e2"][0], p["w1"][0]],[p["n1"][1], p["n2"][1]], linestyle='-', color = self.color, alpha=0.1)
            self.bax.plot([p["e1"][0], p["e2"][0]],[p["s2"][1], p["n1"][1]], linestyle='-', color = self.color, alpha=0.1)
            self.bax.plot([p["w2"][0], p["e1"][0]],[p["s1"][1], p["s2"][1]], linestyle='-', color = self.color, alpha=0.1)
            self.bax.plot([p["w1"][0], p["w2"][0]],[p["n2"][1], p["s1"][1]], linestyle='-', color = self.color, alpha=0.1)


        if pDrawLabels:
            for key in p.keys():
                self.bax.text(p[key][0],p[key][1], key, color = self.color )

    def flipPoints ( self,pPoints):
        for key, point in pPoints.items():
            point[0] = -point[0]

    def rotatePoints( self, pPoints, pRad ):
        #| cos(a)     -sin(a) |
        #| sin(a)      cos(a) |
        #print("rad:\t" + str(pRad) + "\tcos(rad):\t" + str(np.cos(pRad)))
        #print("rad:\t" + str(pRad) + "\tsin(rad):\t" + str(np.sin(pRad)))
        for key, point in pPoints.items():
            p0 = point[0]
            p1 = point[1]
            point[0]=np.cos(pRad) * p0 - np.sin(pRad) * p1
            point[1]=np.sin(pRad) * p0 + np.cos(pRad) * p1

    def movePoints(self, pPoints, pOffset):
        for key, point in pPoints.items():
            point[0] = point[0] + pOffset[0]
            point[1] = point[1] + pOffset[1]

    def createProjection(self, pPoints):
        p = copy.deepcopy(pPoints)

        #Move image to the senso according to the assembly
        if self.orientation == "Standard":
            pass
        elif self.orientation == "Rotated":
            self.rotatePoints(p, np.pi)
        elif self.orientation == "Flipped":
            self.flipPoints(p)
        elif self.orientation == "FlippedAndRotated":
            self.flipPoints(p)
            self.rotatePoints(p, np.pi)

        #Move the sensor to the measured displacement
        self.rotatePoints(p, self.rotation)
        self.movePoints(p,self.offset)

        return p

    def getPointsForAlignment(self):
        p = self.createProjection(self.p)

        #return the needed points to calculate the alignment
        if self.orientation == "Standard":
            return p["0"],p["p1"], p["p2"]
        elif self.orientation == "Rotated":
            return p["0"],p["p3"], p["p4"]
        elif self.orientation == "Flipped":
            return p["0"],p["p4"], p["p3"]
        elif self.orientation == "FlippedAndRotated":
            return p["0"],p["p2"], p["p1"]

    def getDicingEdgePoints(self, pCardinal):
        p = self.createProjection(self.p)

        #Checked
        if pCardinal == "North":
            #Rotated the edge "to the laser measurement region" (south)
            self.rotatePoints(p,np.pi)
            if self.orientation == "Standard":
                return p["n1"], p["n2"]
            elif self.orientation == "Rotated":
                return p["s1"], p["s2"]
            elif self.orientation == "Flipped":
                return p["n2"], p["n1"]
            elif self.orientation == "FlippedAndRotated":
                return p["s2"], p["s1"]


        elif pCardinal == "East":
            self.rotatePoints(p,  - np.pi / 2.0  )
            if self.orientation == "Standard":
                return p["e1"], p["e2"]
            if self.orientation == "Rotated":
                return p["w1"], p["w2"]
            elif self.orientation == "Flipped":
                return p["w2"], p["w1"]
            elif self.orientation == "FlippedAndRotated":
                return p["e2"], p["e1"]

        #Checked
        elif pCardinal == "South":
            if self.orientation == "Standard":
                return p["s1"], p["s2"]
            elif self.orientation == "Rotated":
                return p["n1"], p["n2"]
            elif self.orientation == "Flipped":
                return p["s2"], p["s1"]
            elif self.orientation == "FlippedAndRotated":
                return p["n2"], p["n1"]


        elif pCardinal == "West":
            self.rotatePoints(p,  np.pi / 2.0  )
            if self.orientation == "Standard":
                return p["w1"], p["w2"]
            if self.orientation == "Rotated":
                return p["e1"], p["e2"]
            elif self.orientation == "Flipped":
                return p["e2"], p["e1"]
            elif self.orientation == "FlippedAndRotated":
                return p["w2"], p["w1"]


class Module():
    def __init__( self, pId,pTopSensor, pBottomSensor, pEdgeValues, pBax ):
        print("New module, ID: " + pId)
        self.top = pTopSensor
        self.bottom = pBottomSensor
        self.bax = pBax
        printSteps = False

        #Four laser edge measurements yield 4 rotation,
        #2 y_shift and 2 x_shift values

        #After the values are calculated for each cardinal, the mean value is returned
        results = {}

        if "North" in pEdgeValues.keys():
            self.top.rotation =  0
            self.top.offset =  [0,0]
            #---------------North--------------------------------------------
            top1, top2 = self.top.getDicingEdgePoints("North")
            bot1, bot2 = self.bottom.getDicingEdgePoints("North")
            #self.rotatePoints([top1,top2,bot1,bot2],np.pi)
            m_init = ( (top2[1] - bot2[1]) - (top1[1] - bot1[1]) ) / xActive
            b_init = (top1[1] + top2[1] ) / 2.0 - (bot1[1] + bot2[1]) / 2.0
            m_north = pEdgeValues["North"]["m"]
            b_north = pEdgeValues["North"]["b"]

            #No we know the position of the two sensors in respect to each other solely due to the dicing imperfections
            #Now we start to manipulate the top sensor to match the laser measurement of the edge
            self.top.rotation =  m_north * 10**-6 - m_init
            self.top.offset =  [self.top.offset[0], - (b_north - b_init) ]

            #Get the alignmentvalue from the module.
            rotation, x_shift, y_shift = self.calculateAlignment()
            #Print and store values
            if printSteps:
                print("Alignment in Module. NORTH measurement:")
                print("_______________________________________")
                print("m_init:\t" + str(m_init))
                print("b_init:\t" + str(b_init))
                print("-----------------------------")
                print("m_north:\t" + str(m_north))
                print("b_north:\t" + str(b_north))
                print("-----------------------------")
                print("Rotation:\t" + str(rotation))
                print("y_shift:\t" + str(y_shift))

            results["North"] = {"rotation": rotation, "x_shift": x_shift, "y_shift": y_shift}

        if "East" in pEdgeValues.keys():
            self.top.rotation =  0
            self.top.offset =  [0,0]
            top1, top2 = self.top.getDicingEdgePoints("East")
            bot1, bot2 = self.bottom.getDicingEdgePoints("East")
            #self.rotatePoints([top1,top2,bot1,bot2],np.pi)
            m_init = ( (top2[1] - bot2[1]) - (top1[1] - bot1[1]) ) / yActive
            b_init = (top1[1] + top2[1] ) / 2.0 - (bot1[1] + bot2[1]) / 2.0
            m_east = pEdgeValues["East"]["m"]
            b_east = pEdgeValues["East"]["b"]

            #No we know the position of the two sensors in respect to each other solely due to the dicing imperfections
            #Now we start to manipulate the top sensor to match the laser measurement of the edge
            self.top.rotation =  m_east * 10**-6 - m_init
            self.top.offset =  [- (b_east - b_init), self.top.offset[1]  ]

            #Get the alignmentvalue from the module.
            rotation, x_shift, y_shift = self.calculateAlignment()
            #Print and store values
            if printSteps:
                print("Alignment in Module. EAST measurement:")
                print("_______________________________________")
                print("m_init:\t" + str(m_init))
                print("b_init:\t" + str(b_init))
                print("-----------------------------")
                print("m_east:\t" + str(m_east))
                print("b_east:\t" + str(b_east))
                print("-----------------------------")
                print("Rotation:\t" + str(rotation))
                print("x_shift:\t" + str(x_shift))
            results["East"] = {"rotation": rotation, "x_shift": x_shift, "y_shift": y_shift}

        if "South" in pEdgeValues.keys():
            #reset the previous rotation and shift
            self.top.rotation =  0
            self.top.offset =  [0,0]
            top1, top2 = self.top.getDicingEdgePoints("South")
            bot1, bot2 = self.bottom.getDicingEdgePoints("South")
            m_init = ( (top2[1] - bot2[1]) - (top1[1] - bot1[1]) ) / xActive
            b_init = (top1[1] + top2[1] ) / 2.0 - (bot1[1] + bot2[1]) / 2.0
            m_south = pEdgeValues["South"]["m"]
            b_south = pEdgeValues["South"]["b"]

            #No we know the position of the two sensors in respect to each other solely due to the dicing imperfections
            #Now we start to manipulate the top sensor to match the laser measurement of the edge
            self.top.rotation =  m_south * 10**-6 - m_init
            self.top.offset =  [self.top.offset[0],b_south - b_init]

            #Get the alignmentvalue from the module.
            rotation, x_shift, y_shift = self.calculateAlignment()
            #Print and store values
            if printSteps:
                print("Alignment in Module. SOUTH measurement:")
                print("_______________________________________")
                print("m_init:\t" + str(m_init))
                print("b_init:\t" + str(b_init))
                print("-----------------------------")
                print("m_south:\t" + str(m_south))
                print("b_south:\t" + str(b_south))
                print("-----------------------------")
                print("Rotation:\t" + str(rotation))
                print("y_shift:\t" + str(y_shift))
            results["South"] = {"rotation": rotation, "x_shift": x_shift, "y_shift": y_shift}

        if "West" in pEdgeValues.keys():
            #reset the previous rotation and shift
            self.top.rotation =  0
            self.top.offset =  [0,0]
            top1, top2 = self.top.getDicingEdgePoints("West")
            bot1, bot2 = self.bottom.getDicingEdgePoints("West")
            m_init = ( (top2[1] - bot2[1]) - (top1[1] - bot1[1]) ) / yActive
            b_init = (top1[1] + top2[1] ) / 2.0 - (bot1[1] + bot2[1]) / 2.0
            m_west = pEdgeValues["West"]["m"]
            b_west = pEdgeValues["West"]["b"]

            #No we know the position of the two sensors in respect to each other solely due to the dicing imperfections
            #Now we start to manipulate the top sensor to match the laser measurement of the edge
            self.top.rotation =  m_west * 10**-6 - m_init
            self.top.offset =  [ (b_west - b_init), self.top.offset[1]  ]

            #Get the alignmentvalue from the module.
            rotation, x_shift, y_shift = self.calculateAlignment()
            #Print and store values
            if printSteps:
                print("Alignment in Module. WEST measurement:")
                print("_______________________________________")
                print("m_init:\t" + str(m_init))
                print("b_init:\t" + str(b_init))
                print("-----------------------------")
                print("\tm_west:\t" + str(m_west))
                print("\tb_west:\t" + str(b_west))
                print("-----------------------------")
                print("\tRotation:\t" + str(rotation))
                print("\tx_shift:\t" + str(x_shift))

            results["West"] = {"rotation": rotation, "x_shift": x_shift, "y_shift": y_shift}

        rotationMean =  sum([results[key]["rotation"] for key in results.keys()])/ \
                        len ([results[key]["rotation"] for key in results.keys()])
        xShiftMean =    sum([results[key]["x_shift"] for key in results.keys() if results[key]["x_shift"] != 0] )/ \
                        len([results[key]["x_shift"] for key in results.keys() if results[key]["x_shift"] != 0] )
        yShiftMean =    sum([results[key]["y_shift"] for key in results.keys() if results[key]["y_shift"] != 0] )/ \
                        len([results[key]["y_shift"] for key in results.keys() if results[key]["y_shift"] != 0] )

        #rotationsMean = sum( [item["rotation"] for item in results] )
        #/ len(results)
        #yShiftMean = sum( [item["rotation"]["y_shift"] for item in results if item["rotation"]["y_shift"] != 0] )
        # / len([item["rotation"]["y_shift"] for item in results if item["rotation"]["y_shift"] != 0])
        print("\nCardinal\tRotation\tx_shift\t\ty_shift")
        print("________________________________________________________")
        for key, item in results.items():
            print(key +"\t\t{:.2f}".format(item["rotation"]*10**6) + "\t\t{:.2f}".format(item["x_shift"]) +"\t\t{:.2f}".format(item["y_shift"]) )
            #if key == "North" or key == "South":
                #xShiftMean += item["y_shift"]
        print("--------------------------------------------------------")
        print ("Mean:\t\t{:.2f}".format(rotationMean*10**6) + "\t\t{:.2f}".format(xShiftMean) +"\t\t{:.2f}".format(yShiftMean) + "\n")

        self.top.rotation =  0
        self.top.offset =  [0,0]
        self.top.rotation =  rotationMean
        self.top.offset =  [xShiftMean,yShiftMean]



    def calculateAlignment( self ):
        originBot, nwBot, swBot = self.bottom.getPointsForAlignment()
        originTop, nwTop, swTop = self.top.getPointsForAlignment()
        #Calculate slope with respect to implants
        rotation = - ( nwTop[0]-swTop[0] ) / ( nwBot[1] - swBot[1] )
        x_shift = originTop[0]
        y_shift = originTop[1]
        return rotation, x_shift, y_shift

    def drawModule( self,pDrawLabels ):
        self.top.drawSensor(pDrawLabels = pDrawLabels)
        self.bottom.drawSensor(pDrawLabels = pDrawLabels)

    def rotatePoints( self, pPoints, pRad ):
        #| cos(a)     -sin(a) |
        #| sin(a)      cos(a) |
        for point in pPoints:
            point[0]=np.cos(pRad) * point[0] - np.sin(pRad) * point[1]
            point[1]=np.sin(pRad) * point[0] + np.cos(pRad) * point[1]

if __name__ == "__main__":
    dicingTop = {}
    dicingTop["North"]   = {"m":0, "b":37}
    dicingTop["East"]    = {"m":0, "b":40}
    dicingTop["South"]   = {"m":0, "b":35}
    dicingTop["West"]    = {"m":0, "b":33}

    dicingBottom = {}
    dicingBottom["North"]   = {"m":0, "b":65}
    dicingBottom["East"]    = {"m":0, "b":65}
    dicingBottom["South"]   = {"m":0, "b":65}
    dicingBottom["West"]    = {"m":0, "b":65}

    edgeValues = {}
    edgeValues["North"]   = {"m":0, "b":0}
    edgeValues["East"]    = {"m":0, "b":0}
    edgeValues["South"]   = {"m":0, "b":0}
    edgeValues["West"]    = {"m":0, "b":0}

    """
    dicingTop["North"]   = {"m":4, "b":37}
    dicingTop["East"]    = {"m":-2, "b":40}
    dicingTop["South"]   = {"m":-5, "b":35}
    dicingTop["West"]    = {"m":3, "b":33}

    dicingBottom = {}
    dicingBottom["North"]   = {"m":-5, "b":35}
    dicingBottom["East"]    = {"m":3, "b":37}
    dicingBottom["South"]   = {"m":4, "b":39}
    dicingBottom["West"]    = {"m":7, "b":36}

    edgeValues = {}
    edgeValues["North"]   = {"m":-33, "b":-10}
    edgeValues["East"]    = {"m":-40, "b":5}
    edgeValues["South"]   = {"m":-35, "b":9}
    edgeValues["West"]    = {"m":-33, "b":-4}
    """

    """
    real values module 4 (irrad sensors)
    edgeValues["North"]   = {"m":8, "b":-8}
    edgeValues["East"]    = {"m":5, "b":-8}
    edgeValues["South"]   = {"m":74, "b":-5}
    edgeValues["West"]    = {"m":66, "b":-9}
    """
    #Make the figure to draw the module with broken axes in in X and Y
    fig = plt.figure(figsize=(8.5, 9))
    breaking = 200
    bax = brokenaxes(xlims=( ( -xMid - breaking, -xMid + breaking ), ( +xMid - breaking, +xMid + breaking)), ylims =  ( ( -yMid - breaking, -yMid + breaking ), ( +yMid - breaking, +yMid + breaking)), hspace=.05)

    #Four combinations of sensor orientation inside the module possbile:
    # Top sensor    - Bottom sensor
    #________________________________
    # Standard      - Flipped
    # Rotated       - Flipped
    # Standard      - FlippedAndRotated
    # Rotated       - FlippedAndRotated
    top = Sensor("Top Sensor", dicingTop, "Rotated", 0 , [0,0], "red", bax)
    #top.drawSensor()

    bot = Sensor("Bottom Sensor", dicingBottom, "Flipped", 0, [0,0], "blue", bax)
    #bot.drawSensor()

    module = Module ("SuperDuperModule",top, bot, edgeValues, bax)
    module.drawModule(False)

    try:
        # Put matplotlib.pyplot in interactive mode so that the plots are shown in a background thread.
        while(True):
            plt.show(block=True)
            break
    except KeyboardInterrupt:
        sys.exit(0)
